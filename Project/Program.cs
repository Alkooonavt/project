﻿using System;
using System.Threading.Tasks;

namespace Project
{
    class Program
    {
        static async Task Main(string[] args)
        {
            UserController userController = new UserController();
            var isExit = false;
           while (!isExit)
           {
               Console.Clear();
               Console.WriteLine($"1.Авторизация");
               Console.WriteLine($"2.Регистрация");
               Console.WriteLine("Введите пункт:");
              int.TryParse(Console.ReadLine(),out int key);
              switch (key)
              {
                  case 1:
                      await userController.Login();
                      break;
                  case 2:
                      await userController.Register();
                      break;
                  case 0: isExit = true;
                      break;;
              }
           }
            Console.ReadLine();
        }
    }
}