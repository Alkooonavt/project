﻿using System;
using System.Threading.Tasks;

namespace Project
{
    public class UserController
    {
        private readonly DbController _dbController;

        public UserController()
        {
            _dbController=new DbController();
        }

        public async Task Login()
        {
            Console.Write("Введите логин:");
            var login = Console.ReadLine();
            Console.Write("Введите пароль:");
            var password = Console.ReadLine();
            var user= await _dbController.Get(login, password);
            Console.WriteLine(user != null ? $"Hello {user.Name}" : $"Error login or password");
            Console.ReadKey();
        }

        public async Task Register()
        {
            Console.Write("Введите логин:");
            var login = Console.ReadLine();
            Console.Write("Введите имя:");
            var name = Console.ReadLine();
            Console.Write("Введите пароль:");
            var password = Console.ReadLine();
        var isCreate= await  _dbController.Create(new User(login, name, password));
        Console.WriteLine(isCreate ? $"Успешно" : "Не успешно");
        Console.ReadKey();
        }
    }
}