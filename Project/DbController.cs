﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class DbController
    {
        private string path { get; set; } = "users.txt";
        public async Task<bool> Create(User user)
        {
            try
            {
                await using var writer =new StreamWriter(path,true,Encoding.Default);
                var text = $"{user.Login};{user.Name};{user.Password}";
                await writer.WriteLineAsync(text);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public async Task<User> Get(string login,string password)
        {
            try
            { 
               foreach (var text in await File.ReadAllLinesAsync(path))
               {
                   var user = text.Split(";");
                   if (user[0] == login && user[2] == password)
                   {
                       return new User(user[0],user[1],user[2]);
                   }
               }
               return  null;
            }
            catch (Exception e)
            {
              Console.WriteLine("Error" + e.Message);
              return null;
            }
        }
    }
}