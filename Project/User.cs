﻿namespace Project
{
    public class User
    {
        public User() { }

        public User(string login, string name, string password)
        {
            Login = login;
            Name = name;
            Password = password;
        }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}